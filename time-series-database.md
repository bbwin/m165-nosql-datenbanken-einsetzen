# Zeitreihendatenbank

## Lab Environment

Dieses Labor wird in Docker auf Ihrem Host ausgeführt.

| Rolle             | Hostname / Image | IP[:Port]      |
| ----------------- | ---------------- | -------------- |
| prometheus docker | prometheus       | localhost:9090 |

## Voraussetzungen

- [Docker Desktop](https://www.docker.com/products/docker-desktop/)
- Falls Sie noch ein alte Windows-Version benutzen: [Linux kernel update package](https://learn.microsoft.com/en-us/windows/wsl/install-manual#step-4---download-the-linux-kernel-update-package)
- [Windows Subsystem for Linux](https://learn.microsoft.com/en-us/windows/wsl/install) (WSL 2)
- Mindestens Ubuntu 20.04 aus dem [Windows Microsoft Store](https://apps.microsoft.com/store/detail/ubuntu-20045-lts/9MTTCL66CPXJ)

## Einführung

[Prometheus](https://prometheus.io/) ist ein leistungsfähiges, quelloffenes Überwachungssystem, das Metriken sammelt und in einer Zeitreihendatenbank (**time-series database**) speichert. Es bietet ein multidimensionales Datenmodell, eine flexible Abfragesprache und vielfältige Visualisierungsmöglichkeiten durch Tools wie [Grafana](https://grafana.com/).

Standardmässig exportiert Prometheus nur Metriken über sich selbst (z. B. die Anzahl der Anfragen, die es erhalten hat, seinen Speicherverbrauch usw.). Sie können Prometheus jedoch erheblich erweitern, indem Sie *Exporter* installieren, optionale Programme, die zusätzliche Metriken erzeugen.

Exporter - sowohl die offiziellen, die das Prometheus-Team pflegt, als auch die von der Community bereitgestellten - liefern Informationen über alle mögichen Bereich, von der Infrastruktur über Datenbanken und Webserver bis hin zu Nachrichtensysteme, APIs und mehr.

## Schritt 1 - Prometheus installieren

In diesem Abschnitt installieren Sie den Prometheus-Server mit Docker. Der Prometheus-Server ist das zentrale Element des Prometheus-Ökosystems und ist für das Sammeln und Speichern von Metriken sowie für die Verarbeitung von Expressions und die Generierung von Warnmeldungen verantwortlich.

Die Docker-Container-Images für alle Prometheus-Komponenten werden unter der **[prom](https://hub.docker.com/u/prom/)**-Organisation auf Docker Hub gehostet. Wenn Sie das Docker-Image "prom/prometheus" ohne weitere Optionen ausführen, wird der Prometheus-Server mit einer Beispielkonfigurationsdatei unter "/etc/prometheus/prometheus.yml" im Container gestartet. Ausserdem wird ein Docker-Datenvolumen verwendet, das unter `/prometheus` innerhalb des Containers gemountet ist, um gesammelte Metrikdaten zu speichern. Dieses Datenvolumenverzeichnis ist eigentlich ein Verzeichnis auf dem Host, das Docker beim ersten Start des Containers automatisch erstellt. Die Daten darin werden zwischen den Neustarts desselben Containers beibehalten.

Es gibt mehrere Möglichkeiten, die Standardkonfigurationsdatei ausser Kraft zu setzen. Beispielsweise kann eine benutzerdefinierte Konfigurationsdatei vom Host-Dateisystem als Docker-Datenvolumen an den Container übergeben werden, oder Sie können einen abgeleiteten Docker-Container mit Ihrer eigenen Konfigurationsdatei erstellen, die in das Container-Image integriert ist. Für diese Übung werden wir eine Konfigurationsdatei aus dem Host-Dateisystem übergeben.

Starten Sie zunächst Ihr **WSL2-Terminal** unter Ubuntu 20.04 auf Ihrem Notebook. Dies kann je nach Ihrer Einrichtung variieren.

Erstellen Sie eine minimale Prometheus-Konfigurationsdatei `prometheus.yml`. 

```bash
nano prometheus.yml
```

Fügen Sie der Datei den folgenden Inhalt hinzu

```bash
global:
  scrape_interval:     15s # By default, scrape targets every 15 seconds.

# A scrape configuration containing exactly one endpoint to scrape:
# Here it's Prometheus itself.
scrape_configs:

  - job_name: 'prometheus'

    # Override the global default and scrape targets from this job every 5 seconds.
    scrape_interval: 5s

    static_configs:
      - targets: ['localhost:9090']
```

Diese Beispielkonfiguration lässt Prometheus Metriken von sich selbst abrufen (da Prometheus auch Metriken über sich selbst in einem Prometheus-kompatiblen Format bereitstellt)

Starten Sie den Prometheus-Docker-Container mit der externen Konfigurationsdatei:

```bash
docker run -d -p 9090:9090 -v $(pwd)/prometheus.yml:/etc/prometheus/prometheus.yml prom/prometheus
```

Dieser Befehl ist recht lang und enthält viele Befehlszeilenoptionen. Schauen wir ihn uns im Detail an:

- Die Option `-d` startet den Prometheus-Container im Detached Mode, d.h. der Container wird im Hintergrund gestartet und nicht durch Drücken von `CTRL+C` beendet.
- Die Option `-p 9090:9090` macht Prometheus über den Port 9090 verfügbar.
- Die Option `-v [...]` mountet die Konfigurationsdatei `prometheus.yml` vom Host-Dateisystem an den Ort im Container, an dem Prometheus sie erwartet (`/etc/prometheus/prometheus.yml`).

Sie können nun alle laufenden Docker-Container mit dem folgenden Befehl auflisten:

```bash
docker ps
```

Für den Prometheus-Docker-Container sehen Sie zum Beispiel etwa das Folgende:

```bash
CONTAINER ID   IMAGE             COMMAND                  CREATED          STATUS          PORTS                                       NAMES
44a1720c1af3   prom/prometheus   "/bin/prometheus --c…"   53 seconds ago   Up 52 seconds   0.0.0.0:9090->9090/tcp, :::9090->9090/tcp   peaceful_roentgen
```

Anhand der Container-ID, die in der Ausgabe von "docker ps" angezeigt wird, können Sie die Protokolle des laufenden Prometheus-Servers mit dem Befehl "docker logs <container_id>" einsehen:

```bash
docker logs 44a1720c1af3
```

Wie bei vielen anderen Befehlen in Linux, benötigt Docker nur den eindeutigen Teil der Container-ID. Der Befehl `docker logs 44a1720c1af3` kann auch wie `docker logs 44a` geschrieben werden

Um herauszufinden, wo auf dem Dateisystem des Hosts das Metrics-Storage-Volume gespeichert ist, können Sie den folgenden Befehl mit Ihrer Container-ID ausführen:

```bash
docker inspect 44a1720c1af3
```

Suchen Sie in der Ausgabe einen Abschnitt, der ähnlich aussieht wie dieser:

```bash
"Mounts": [
    {
        "Type": "volume",
        "Name": "df1687928dd189c6082f44e3d8c0f0cfedd4c547d67e0f5672ec00929a83cebb",
        "Source": "/var/lib/docker/volumes/df1687928dd189c6082f44e3d8c0f0cfedd4c547d67e0f5672ec00929a83cebb/_data",
        "Destination": "/prometheus",
        "Driver": "local",
        "Mode": "",
        "RW": true,
        "Propagation": ""
    },
    {
        "Type": "bind",
        "Source": "/home/dominic/prometheus.yml",
        "Destination": "/etc/prometheus/prometheus.yml",
        "Mode": "",
        "RW": true,
        "Propagation": "rprivate"
    }
],
```

In diesem Beispiel werden die Metriken in `/var/lib/docker/volumes/df1687928dd189c6082f44e3d8c0f0cfedd4c547d67e0f5672ec00929a83cebb/_data` auf dem Host-System gespeichert. Dieses Verzeichnis wurde von Docker beim ersten Start des Prometheus-Containers automatisch erstellt. Es wird in das Verzeichnis "/prometheus" im Container eingebunden. Die Daten in diesem Verzeichnis werden über Neustarts desselben Containers hinweg beibehalten.

Sie sollten nun in der Lage sein, Ihren Prometheus-Server unter `http://0.0.0.0:9090/` zu erreichen. Überprüfen Sie, ob er Metriken über sich selbst sammelt, indem Sie `http://0.0.0.0:9090/status` aufrufen. 

Überprüfen Sie den Abschnitt **Status / Targets** . Die Spalte **State** für Ihren Metrik-Endpunkt sollte den Status des Ziels als **UP** anzeigen.

![](images/prometheus-targets.png)

## Schritt 2 - Verwendung des expression browser

Lassen Sie uns die Daten untersuchen, die Prometheus über sich selbst gesammelt hat. Um den in Prometheus integrierten expression browser zu verwenden, navigieren Sie zu http://0.0.0.0:9090/graph und wählen Sie die Ansicht "Table" in der Registerkarte "Graph".

Wie Sie der Seite `0.0.0.0:9090/metrics` entnehmen können, gibt es eine Metrik, die Prometheus über sich selbst exportiert: `prometheus_target_interval_length_seconds` (die tatsächliche Zeitspanne zwischen Abfragen). 

Geben Sie dies in der expression-Konsole ein:

```bash
prometheus_target_interval_length_seconds
```

Dies sollte eine Reihe verschiedener Zeitreihen zurückgeben (zusammen mit dem jeweils letzten aufgezeichneten Wert), jede mit dem Metriknamen "prometheus_target_interval_length_seconds", aber mit unterschiedlichen Bezeichnungen.

![](images/prometheus_target_interval_length_seconds.png)

Wenn wir nur an den 99th percentile-Latenzen interessiert sind, können wir diese Abfrage verwenden:

```bash
prometheus_target_interval_length_seconds{quantile="0.99"}
```

![](images/prometheus-quantile.png)

Um die Anzahl der zurückgegebenen Zeitreihen zu zählen, könnten Sie schreiben:

```bash
count(prometheus_target_interval_length_seconds)
```

![](images/prometheus-count.png)

Um Ausdrücke grafisch darzustellen, verwenden Sie die Registerkarte "Graph". Geben Sie z. B. den folgenden Ausdruck ein, um die Rate der pro Sekunde erstellten Chunks im gescrapten Prometheus grafisch darzustellen:

```bash
rate(prometheus_tsdb_head_chunks_created_total[1m])
```

![](images/prometheus-rate.png)

## Schritt 3 - Einrichten des Node Exporter

In diesem Abschnitt werden wir den Prometheus Node Exporter installieren. Der Node Exporter ist ein Serverdienst, der Prometheus-Metriken über den Host-Rechner (Node), auf dem er läuft, zur Verfügung stellt. Dazu gehören Metriken über die Dateisysteme des Rechners, Netzwerkgeräte, Prozessornutzung, Speichernutzung und mehr.

Beachten Sie, dass die Ausführung des Node Exporter in Docker einige Herausforderungen mit sich bringt, da sein einziger Zweck darin besteht, Metriken über den Host, auf dem er ausgeführt wird, offenzulegen. Wenn wir ihn auf Docker ohne weitere Optionen ausführen, führt das Namespacing von Ressourcen wie dem Dateisystem und den Netzwerkgeräten in Docker dazu, dass nur Metriken über die Umgebung des Containers exportiert werden, die sich von der Umgebung des Hosts unterscheiden. Daher ist es in der Regel empfehlenswert, den Node Exporter direkt auf dem Hostsystem ausserhalb von Docker auszuführen.

Diesen Teil des Praktikums werden Sie selbstständig erarbeiten. Der [Node Exporter guide](https://prometheus.io/docs/guides/node-exporter/) bietet Ihnen einen guten Ausgangspunkt.

Die erforderlichen Schritte umfassen:

- Laden Sie `node-exporter` herunter und führen Sie ihn aus

- Konfigurieren Sie Ihre `prometheus.yml` auf Ihrem Notebook, so dass es zusätzlich Ihren `node-exporter` scrapen kann.

  **Achtung**: Studieren Sie den Aufbau der Datei genau. Der Kontext "scrape_config" darf nur einmal vorkommen in dieser Datei. Achten Sie auch auf die korrekte Einrückung.

  ```bash
  scrape_configs:
  - job_name: node
    static_configs:
    - targets: ['10.0.0.10:9100']
  ```

- Starten Sie Ihren Docker-Container mit `docker restart <container_id>` neu:
  ```bash
  docker restart 44a1720c1af3
  ```

- Prüfen Sie, ob Ihre neuen Ziele verfügbar sind. (Hinweis: Der Status sollte **UP** sein)
  ![](images/prometheus-nodeexporter.png)

- Erkunden Sie die neu hinzugefügten Metriken mithilfe der Prometheus-Benutzeroberfläche. Navigieren Sie in Ihrem Browser zu http://0.0.0.0:9090/graph und verwenden Sie die Expression-Leiste, um einige nützliche Diagramme zu erstellen.

## Schritt 4 - Lerntagebuch

Das Ziel dieser Übung war es, Ihnen einen Einblick in time-series-Datenbanken zu geben. Nachdem Sie ein Gefühl dafür bekommen haben, wie man sie installiert und benutzt, sollten Sie nun in der Lage sein, einige Fragen zu recherchieren und sie in Ihrem Studientagebuch zu notieren.

- Was sind die Vor- und Nachteile von [Zeitseriendatenbanken](https://en.wikipedia.org/wiki/Time_series_database)? Wie unterscheiden sie sich von Key-Value-Stores und relationalen Datenbanken?
- Prometheus ist nur ein bekanntes Produkt. Recherchieren Sie andere, wählen Sie 2-3 andere Produkte aus und beschreiben Sie, wofür sie verwendet werden.
- Prometheus verwendet eine Pull-Methode, um seine Daten zu sammeln (Prometheus verbindet sich mit dem `/metrics` API-Endpunkt). Andere Systeme wie Graphite verwenden eine Push-Methode. Welche Methode ist besser?
